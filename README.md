# cbr2cbz

A simple script to convert .cbr (rar-based) comic book archives to .cbz (zip-based) comic book archives.

## Requirements

- You must have [unrar](https://www.rarlab.com/) and ``zip`` installed. Zip is probably alreadyinstalled, and Unrar is probably available from your software repository..

- This is a Bash script. It assumes Bash is in your PATH. If not, launch the script with Bash specifically (``bash ./cbr2cbz path/to/book.cbr`` for example).

- This is for Linux/BSD/Solaris/Illumos systems. I cannot guarantee it works on any other platform because I do not have access to them. It might work, but you may have to fix stuff to make it work right. If you do that, send me a patch and I'll gladly merge!
